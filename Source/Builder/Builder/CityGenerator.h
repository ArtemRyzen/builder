// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CanGenerateCity.h"
#include "Kismet/KismetSystemLibrary.h"
#include "UObject/NoExportTypes.h"
#include "CityGenerator.generated.h"

/**
 * Class to generate a elven city
 */
UCLASS(Blueprintable, BlueprintType)
class BUILDER_API UElvenCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()

public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateCastle() override;
	virtual void GenerateHourse() override;
	/** END: ICanGenerateCity interface implementation */

protected:
	void SpawnRoot() { UKismetSystemLibrary::PrintString(this, "SpawnRoot In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnGrass() { UKismetSystemLibrary::PrintString(this, "SpawnGrass In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnTrees() { UKismetSystemLibrary::PrintString(this, "SpawnTrees In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnRivers() { UKismetSystemLibrary::PrintString(this, "SpawnRivers In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnTreeCastle() { UKismetSystemLibrary::PrintString(this, "SpawnTreeCastle In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnTreeHourse() { UKismetSystemLibrary::PrintString(this, "SpawnTreeHourse In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };

private:
	/** The root of the city. */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "City", meta = (AllowPrivateAccess = "True"))
		AActor* CityRoot;
};

inline UObject* UElvenCityGenerator::GetResult() const
{
	UKismetSystemLibrary::PrintString(this, "GetResult In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);
	return CityRoot;
}

inline void UElvenCityGenerator::GenerateGrounds()
{
	UKismetSystemLibrary::PrintString(this, "GenerateGrounds In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

	SpawnRoot();
	SpawnGrass();
	SpawnTrees();
	SpawnRivers();
}

inline void UElvenCityGenerator::GenerateCastle()
{
	UKismetSystemLibrary::PrintString(this, "GenerateCastle In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

	SpawnTreeCastle();
}

inline void UElvenCityGenerator::GenerateHourse()
{
	UKismetSystemLibrary::PrintString(this, "GenerateHourse In ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

	SpawnTreeHourse();
}


/**
 * Class to generate a necromantic city
 */
UCLASS(Blueprintable, BlueprintType)
class BUILDER_API UNecroCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()
public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateCastle() override {};
	virtual void GenerateHourse() override;
	/** END: ICanGenerateCity interface implementation */

protected:
	void SpawnRoot() { UKismetSystemLibrary::PrintString(this, "SpawnRoot In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnDeadGround() { UKismetSystemLibrary::PrintString(this, "SpawnDeadGround In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnStyx() { UKismetSystemLibrary::PrintString(this, "SpawnStyx In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnLeta() { UKismetSystemLibrary::PrintString(this, "SpawnLeta In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnCemetery() { UKismetSystemLibrary::PrintString(this, "SpawnCemetery In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnDom() { UKismetSystemLibrary::PrintString(this, "SpawnDom In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };
	void SpawnTombs() { UKismetSystemLibrary::PrintString(this, "SpawnTombs In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f); };

private:
	/** The root of the city */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "City", meta = (AllowPrivateAccess = "True"))
		AActor* CityRoot;	
};

inline UObject* UNecroCityGenerator::GetResult() const
{
	UKismetSystemLibrary::PrintString(this, "GetResult In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

	return CityRoot;
}

inline void UNecroCityGenerator::GenerateGrounds()
{
	UKismetSystemLibrary::PrintString(this, "GenerateGrounds In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);
	SpawnRoot();
	SpawnDeadGround();
	SpawnStyx();
	SpawnLeta();
}

inline void UNecroCityGenerator::GenerateHourse()
{
	UKismetSystemLibrary::PrintString(this, "GenerateHourse In NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);
	SpawnCemetery();
	SpawnDom();
	SpawnTombs();
}

/**
 * Class to generate some basic quest structure for given area.
 */
UCLASS()
class BUILDER_API UCityQuestGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()
public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override { return QuestRoot; }
	virtual void GenerateCastle() override {};
	virtual void GenerateGrounds() override {};
	virtual void GenerateHourse() override {};
	/** END: ICanGenerateCity interface implementation */

private:
	UObject* QuestRoot;
};
